package ru.edu;

import java.time.LocalDateTime;
import java.util.UUID;

public class Product {

    public Product() {
    }

    private UUID id;
    private String name;
    private String description;
    private ProductCategory productCategory;
    private LocalDateTime manufactureDateTime;
    private String manufacturer;
    private Boolean hasExpiryTime;
    private int stock;

    public static class Builder {
        private Product newProduct;

        public Builder() {
            newProduct = new Product();
        }

        public Builder setId(UUID id) {
            newProduct.id = id;
            return this;
        }

        public Builder setName(String name) {
            newProduct.name = name;
            return this;
        }

        public Builder setDescription(String description) {
            newProduct.description = description;
            return this;
        }

        public Builder setProductCategory(ProductCategory productCategory) {
            newProduct.productCategory = productCategory;
            return this;
        }

        public Builder setManufactureDateTime(LocalDateTime manufactureDateTime) {
            newProduct.manufactureDateTime = manufactureDateTime;
            return this;
        }

        public Builder setManufacturer(String manufacturer) {
            newProduct.manufacturer = manufacturer;
            return this;
        }

        public Builder setHasExpiryTime(Boolean hasExpiryTime) {
            newProduct.hasExpiryTime = hasExpiryTime;
            return this;
        }

        public Builder setStock(int stock) {
            newProduct.stock = stock;
            return this;
        }

        public Product build(){
            return newProduct;
        }
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public LocalDateTime getManufactureDateTime() {
        return manufactureDateTime;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Boolean getHasExpiryTime() {
        return hasExpiryTime;
    }

    public int getStock() {
        return stock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category=" + productCategory +
                ", manufactureDateTime=" + manufactureDateTime +
                ", manufacturer='" + manufacturer + '\'' +
                ", hasExpiryTime=" + hasExpiryTime +
                ", stock=" + stock +
                '}';
    }
}
